"use strict";

class SlidesFlow {

	constructor(container, config = false) {
		this.config = {
			cooldown: config.cooldown || false,
			transition: config.transition || false,
		};

		this.DOM = SlidesFlow.createMarkup(container, this.config.transition);
		this.currentSlide = 0;
		this.totalSlides = this.DOM.slides.length;

		document.addEventListener('wheel', event => this.onMouseWheel(event), {
			passive: true
		});

		this.cooldown = null;
	}

	onMouseWheel(event) {

		if (this.config.cooldown) {
			!!this.cooldown && clearTimeout(this.cooldown);
			this.cooldown = setTimeout(() => {
				clearTimeout(this.cooldown);
				this.onScroll(event)
			}, this.config.cooldown)
		} else {
			this.onScroll(event)
		}
	}

	onScroll(event) {

		let direction = (event.deltaY > 0) ? 1 : -1;
		let nextSlide = this.currentSlide + direction;
		let scrollAllowed = (nextSlide < this.totalSlides && nextSlide > -1);

		if (scrollAllowed) {

			this.currentSlide = nextSlide;
			window.requestAnimationFrame(() => {
				this.DOM.wrapper.style.transform = `translateX( -${100*this.currentSlide}%)`;
			})
		}

	}
	static createMarkup(container, transition = false) {

		let wrapper = document.createElement('div');
		wrapper.classList.add('slides-flow__wrap');
		if (transition) wrapper.style.transition = `transform ${transition}`;

		const slides = Array.from(container.children);
		let appendSlides = [];
		while(slides.length > 0) {
			if (window.innerWidth <= 768) {
				appendSlides.push(slides.splice(0,1));
			} else if (window.innerWidth <= 1400) {
				appendSlides.push(slides.splice(0,2));
			} else if (window.innerWidth < 1920) {
				appendSlides.push(slides.splice(0,3));
			} else if (window.innerWidth >= 1920 ) {
				appendSlides.push(slides.splice(0,6));
			}
		}
		let wrappedSlides = appendSlides.map(slide => {
			let slideWrap = document.createElement('div');
			slideWrap.classList.add('slides-flow__slide');
			slide.forEach(el => slideWrap.appendChild(el));
			return slideWrap;
		});

		wrappedSlides.forEach(wrappedSlide => {
			wrapper.appendChild(wrappedSlide);
		});


		container.appendChild(wrapper);

		return {
			container,
			wrapper,
			slides: wrappedSlides
		}
	}

}


const slidesFlowElement = document.querySelectorAll('.slides-flow');

Array.from(slidesFlowElement).forEach( element => new SlidesFlow(element, {
	cooldown: 300,
	transition: '0.25s ease-out'
}));

